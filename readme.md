# BAT-BACKEND

The project used to show my development process on the Battery State Demo

1. Build a backend template

  - Create backend folder
  - Define

        src folder -> source code
        test folder -> unit test file
        script folder -> setup file
        install convert module to switch ES6/7 to nodejs version 

  - Execute script in the backend folder

        // install backend project module
        $ npm install
        // execute app in the development mode
        $ npm run dev
        // execute unit test
        $ npm run test
        // compile app to production code
        $ npm run build

Build a backend template

- Create backend folder
- Define

      src folder -> source code
      test folder -> unit test file
      script folder -> setup file
      install convert module to switch ES6/7 to nodejs version 

- Execute script in the backend folder

      // install backend project module
      $ npm install
      // execute app in the development mode
      $ npm run dev
      // execute unit test
      $ npm run test
      // compile app to production code
      $ npm run build

- Change Log

  1. Build Battery State Machine
  2. Build Battery Observer
  3. Build Web Service
