import Battery from './battery';
import Batstate from './batstate';

import BatSubject from './batsubject';
import BatObserver from './batobserver';

/** Class representing a Battery Simulator */
class BatSimulator {
  static get Battery() { return Battery; }

  static get BatObserver() { return BatObserver; }

  /**
   * Create a BatSimulator
   * @param {object} batOpts - Battery parameters
   */
  constructor(batOpts = {}) {
    this.bat = new Battery(batOpts);
    this.stableState = new Batstate.StableState(this);
    this.chargingState = new Batstate.ChargingState(this);
    this.dischargingState = new Batstate.DischargingState(this);
    this.alarmState = new Batstate.AlarmState(this);
    this.batSubject = new BatSubject();

    this.setBatState(this.getStableState());
  }

  /**
   * Register Consumer
   */
  register(consumer) {
    return this.batSubject.subscribe(consumer);
  }

  /**
   * Unregister Consumer
   */
  unregister(consumer) {
    this.batSubject.unsubscribe(consumer);
  }

  /**
   * Refresh Battery Information
   */
  update() {
    const info = this.batState.update();

    this.batSubject.publish(info, this.bat.getInfo());

    return info;
  }

  /**
   * Get Battery Object
   * @returns {object} - Battery Object
   */
  getBattery() {
    return this.bat;
  }

  /**
   * Set current battery state
   * @param {object} state - Battery state(StableState, ChargingState, DischargingState, AlarmState)
   */
  setBatState(state = this.batState) {
    this.batState = state;
  }

  /**
   * Get stable state
   * @returns {object} - Battery State Object
   */
  getStableState() {
    return this.stableState;
  }

  /**
   * Get charging state
   * @returns {object} - Battery State Object
   */
  getChargingState() {
    return this.chargingState;
  }

  /**
   * Get discharging state
   * @returns {object} - Battery State Object
   */
  getDischargingState() {
    return this.dischargingState;
  }

  /**
   * Get alarm state
   * @returns {object} - Battery State Object
   */
  getAlarmState() {
    return this.alarmState;
  }
}

export default BatSimulator;
