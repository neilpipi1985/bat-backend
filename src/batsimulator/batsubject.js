/** Class representing a Battery Subject */
class BatSubject {
  /**
   * Create a BatSubject
   */
  constructor() {
    this.consumers = [];
  }

  /**
   * Subscribe Battery Subject
   * @param {object} consumer - Battery Observer
   */
  subscribe(consumer) {
    this.consumers.push(consumer);
  }

  /**
   * Unsubscribe Battery Subject
   * @param {object} consumer - Battery Observer
   */
  unsubscribe(consumer) {
    const index = this.consumers.indexOf(consumer);
    if (index > -1) {
      this.consumers.splice(index, 1);
    }
  }

  /**
   * Publish Battery Subject
   * @param {object} info - state and event
   * @param {object} data - Battery information
   */
  publish(info = {}, data = {}) {
    for (let i = 0; i < this.consumers.length; i += 1) {
      this.consumers[i].update('report', data);
      if (info.notify) {
        this.consumers[i].update(info.notify);
      }
    }
  }
}

export default BatSubject;
