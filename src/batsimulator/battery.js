/** Class representing a Battery */
class Battery {
  /**
   * Create a Battery
   * @param {object} opts - Battery parameters
   */
  constructor(opts = {}) {
    this.deviceId = opts.deviceId || '2219010001';
    this.deviceType = opts.deviceType || 'BT_DEMO';

    this.DESIGN_VOLT = opts.DESIGN_VOLT || 18000;
    this.MAX_CELL_VOLT = opts.MAX_CELL_VOLT || 4150;
    this.MIN_CELL_VOLT = opts.MIN_CELL_VOLT || 2900;
    this.OV_TH = opts.OV_TH || 4200;
    this.UV_TH = opts.UV_TH || 2850;

    this.SOC = opts.SOC || 100;
    this.CURRENT = opts.CURRENT || 0;
    this.TEMPERATURE = opts.TEMPERATURE || 25.5;
    this.CELL_VOLT = opts.CELL_VOLT || [4150, 4150, 4150, 4150];
    this.TOTAL_VOLT = 0;
    this.DELTA_VOLT = 0;
    this.ALARM_FLAG = opts.ALARM_FLAG || {
      OV: false,
      UV: false
    };
    this.refreshVoltValue();
  }

  /**
   * Refresh Volt Value
   */
  refreshVoltValue() {
    let totalVolt = 0;
    let minVolt = this.CELL_VOLT[0] || 0;
    let maxVolt = this.CELL_VOLT[0] || 0;
    for (let i = 0; i < this.CELL_VOLT.length; i += 1) {
      totalVolt += this.CELL_VOLT[i];
      if (minVolt > this.CELL_VOLT[i]) {
        minVolt = this.CELL_VOLT[i];
      }
      if (maxVolt < this.CELL_VOLT[i]) {
        maxVolt = this.CELL_VOLT[i];
      }
    }

    this.TOTAL_VOLT = totalVolt;
    this.DELTA_VOLT = maxVolt - minVolt;
  }

  /**
   * Get Battery Information
   * @returns {object} - Battery Information
   */
  getInfo() {
    return {
      tag: {
        deviceId: this.deviceId,
        deviceType: this.deviceType,
        DESIGN_VOLT: this.DESIGN_VOLT,
      },
      data: {
        SOC: this.SOC,
        CURRENT: this.CURRENT,
        TEMPERATURE: this.TEMPERATURE,
        CELL_VOLT: this.CELL_VOLT,
        TOTAL_VOLT: this.TOTAL_VOLT,
        DELTA_VOLT: this.DELTA_VOLT,
        ALARM_FLAG: this.ALARM_FLAG
      }
    };
  }
}

export default Battery;
