import BaseState from './basestate';

const CLASS_NAME = 'DischargingState';

/** Class representing a Discharging Battery State */
class DischargingState extends BaseState {
  /**
   * Create a Discharging Battery State
   * @param {object} batSimulator - Battery Simulator
   */
  constructor(batSimulator) {
    super(batSimulator, CLASS_NAME);
  }

  /**
   * Refresh Battery Data
   * @param {object} bat - Battery Object
   * @returns {object} - Refresh Battery Object
   */
  refreshBatteryData(bat = (new BaseState.Battery())) {
    bat.SOC -= (Math.floor(Math.random() * 4) + 5);
    bat.SOC = (bat.SOC < 0) ? 0 : bat.SOC;
    bat.CURRENT = (Math.floor(Math.random() * 10000) + 1) * -1;
    const cells = bat.CELL_VOLT;
    let isOV = false;
    for (let i = 0; i < cells.length; i += 1) {
      cells[i] -= (Math.floor(Math.random() * 20) + 80);
      if (cells[i] <= bat.UV_TH) {
        bat.ALARM_FLAG.UV = true;
        bat.CURRENT = 0;
      } else if (cells[i] <= bat.MIN_CELL_VOLT) {
        bat.CURRENT = 0;
      }

      isOV = isOV || (cells[i] >= bat.OV_TH);
    }
    bat.ALARM_FLAG.OV = isOV;
    bat.refreshVoltValue();

    return bat;
  }

  /**
   * Refresh Battery State
   * @param {object} bat - Battery Object
   * @returns {object} - Battery State and Notification
   */
  refreshBatteryState(bat = (new BaseState.Battery())) {
    const info = {
      state: this.getClassName()
    };
    if (bat.ALARM_FLAG.UV) {
      info.state = 'AlarmState';
      info.notify = 'UV Alarm';
    } else if (bat.SOC < 1 || bat.CURRENT === 0) {
      info.state = 'StableState';
      info.notify = 'Stop Discharging';
    }

    return info;
  }
}

export default DischargingState;
