import BaseState from './basestate';

const CLASS_NAME = 'StableState';
const RANDOM_STATE_LIST = ['StableState', 'ChargingState', 'DischargingState'];

/** Class representing a Stable Battery State */
class StableState extends BaseState {
  /**
   * Create a Stable Battery State
   * @param {object} batSimulator - Battery Simulator
   */
  constructor(batSimulator) {
    super(batSimulator, CLASS_NAME);
  }

  /**
   * Refresh Battery State
   * @param {object} bat - Battery Object
   * @returns {object} - Battery State and Notification
   */
  refreshBatteryState(bat = (new BaseState.Battery())) {
    const info = {
      state: this.getClassName()
    };
    if (bat.SOC > 70) {
      info.state = 'DischargingState';
    } else if (bat.SOC < 30) {
      info.state = 'ChargingState';
    } else {
      info.state = RANDOM_STATE_LIST[Math.floor(Math.random() * RANDOM_STATE_LIST.length)];
    }

    if (info.state === 'ChargingState') {
      info.notify = 'Start Charging';
    } else if (info.state === 'DischargingState') {
      info.notify = 'Start Discharging';
    }

    return info;
  }
}

export default StableState;
