import BatSimulator from '../index';

const CLASS_NAME = 'BaseState';

/** Class representing a Basic Battery State */
class BaseState {
  static get Battery() { return BatSimulator.Battery; }

  /**
   * Create a Battery State
   * @param {object} batSimulator - Battery Simulator
   * @param {string} className - The class name
   */
  constructor(batSimulator, className = CLASS_NAME) {
    if (!batSimulator) {
      throw Error('batSimulator is empty');
    }
    this.batSimulator = batSimulator;
    this.className = className;
  }

  /**
   * Get class name
   */
  getClassName() {
    return this.className;
  }

  /**
   * Refresh Battery Data
   * @param {object} bat - Battery Object
   * @returns {object} - Refresh Battery Object
   */
  refreshBatteryData(bat = (new BaseState.Battery())) {
    return bat;
  }

  /**
   * Refresh Battery State
   * @param {object} bat - Battery Object
   * @returns {object} - Battery State and Notification
   */
  refreshBatteryState(bat = (new BaseState.Battery())) {
    return { state: this.getClassName() };
  }

  /**
   * Update Battery
   * @returns {object} - Battery State and Notification
   */
  update() {
    const bat = this.batSimulator.getBattery();

    this.refreshBatteryData(bat);
    const info = this.refreshBatteryState(bat);
    if (info.state && info.state !== this.getClassName()) {
      this.batSimulator.setBatState(this.batSimulator[`get${info.state}`]());
    }
    return info;
  }
}

export default BaseState;
