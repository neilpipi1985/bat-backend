import BaseState from './basestate';

const CLASS_NAME = 'AlarmState';

/** Class representing a Discharging Alarm Battery State */
class AlarmState extends BaseState {
  /**
   * Create a Discharging Alarm Battery State
   * @param {object} batSimulator - Battery Simulator
   */
  constructor(batSimulator) {
    super(batSimulator, CLASS_NAME);
  }

  /**
   * Refresh Battery State
   * @param {object} bat - Battery Object
   * @returns {string} - Refresh Battery State
   */
  refreshBatteryState(bat = (new BaseState.Battery())) {
    const info = {
      state: this.getClassName()
    };

    const random = Math.floor(Math.random() * 4);
    if (random === 0) {
      info.state = bat.ALARM_FLAG.UV ? 'ChargingState' : 'DischargingState';
      info.notify = bat.ALARM_FLAG.UV ? 'Start Charging' : 'Start Discharging';
    }

    return info;
  }
}

export default AlarmState;
