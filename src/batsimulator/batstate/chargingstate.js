import BaseState from './basestate';

const CLASS_NAME = 'ChargingState';

/** Class representing a Charging Battery State */
class ChargingState extends BaseState {
  /**
   * Create a Charging Battery State
   * @param {object} batSimulator - Battery Simulator
   */
  constructor(batSimulator) {
    super(batSimulator, CLASS_NAME);
  }

  /**
   * Refresh Battery Data
   * @param {object} bat - Battery Object
   * @returns {object} - Refresh Battery Object
   */
  refreshBatteryData(bat = (new BaseState.Battery())) {
    bat.SOC += (Math.floor(Math.random() * 4) + 5);
    bat.SOC = (bat.SOC > 99) ? 100 : bat.SOC;
    bat.CURRENT = (Math.floor(Math.random() * 10000) + 1);
    const cells = bat.CELL_VOLT;
    let isUV = false;
    for (let i = 0; i < cells.length; i += 1) {
      cells[i] += (Math.floor(Math.random() * 20) + 80);
      if (cells[i] >= bat.OV_TH) {
        bat.ALARM_FLAG.OV = true;
        bat.CURRENT = 0;
      } else if (cells[i] >= bat.MAX_CELL_VOLT) {
        bat.CURRENT = 0;
      }

      isUV = isUV || (cells[i] <= bat.UV_TH);
    }
    bat.ALARM_FLAG.UV = isUV;
    bat.refreshVoltValue();

    return bat;
  }

  /**
   * Refresh Battery State
   * @param {object} bat - Battery Object
   * @returns {string} - Refresh Battery State
   */
  refreshBatteryState(bat = (new BaseState.Battery())) {
    const info = {
      state: this.getClassName()
    };
    if (bat.ALARM_FLAG.OV) {
      info.state = 'AlarmState';
      info.notify = 'OV Alarm';
    } else if (bat.SOC > 99 || bat.CURRENT === 0) {
      info.state = 'StableState';
      info.notify = 'Stop Charging';
    }

    return info;
  }
}

export default ChargingState;
