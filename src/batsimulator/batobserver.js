/** Class representing a Battery Observer */
class BatObserver {
  /**
   * Create a BatObserver
   */
  constructor() {
    this.trigger = () => {
      console.log('Not Implement trigger function');
    };
  }

  /**
   * Update data to Battery subject
   * @param {object} info - state and event
   * @param {object} data - Battery information
   */
  update(state, data = {}) {
    if (this.trigger) {
      this.trigger(state, data);
    }
  }
}

export default BatObserver;
