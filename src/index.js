import path from 'path';

import Koa from 'koa';
import koaLogger from 'koa-logger';
import koaBodyParser from 'koa-bodyparser';
import KoaStatic from 'koa-static';
import KoaRouter from 'koa-router';

import SocketIO from 'socket.io';

import BatSimulator from './batsimulator';

const WEB_RES_PATH = path.resolve(__dirname, (process.env.WEB_RES_PATH || './public'));

const webApp = new Koa();
let webSocket;

/** Class representing a App Start Class */
class Index {
  /**
   * Start WebSocket Service
   */
  static startWebSocketService(server) {
    webSocket = new SocketIO(server, { path: '/api/socket' });

    webSocket.on('connection', (socket) => {
      console.log('socket connection');
      socket.on('disconnect', () => {
        console.log('a socket disconnect');
      });
    });
    console.log('WebSocket listening on "/api/socket"');
  }

  /**
   * Start Web Service
   */
  static async startWebService(opts = {}) {
    webApp.use(koaLogger());
    webApp.use(koaBodyParser());

    webApp.use(KoaStatic(WEB_RES_PATH));

    // const router = new KoaRouter();
    // router.get('/', async (ctx) => {
    //   ctx.body = 'App is Running';
    // });
    // webApp.use(router.routes());

    const port = opts.port || 8080;
    const service = webApp.listen(port);

    console.log(`Web Service listening on port(${port})`);
    Index.startWebSocketService(service);
  }

  /**
   * Start Battery Report Service
   */
  static async startBatService() {
    const batSimulator = new BatSimulator();
    const batObserver = new BatSimulator.BatObserver();
    batObserver.trigger = (name, data) => {
      console.log(`${(new Date()).toISOString()}: ${name})`);
      console.log(JSON.stringify(data, null, 2));
      if (webSocket) {
        if (name === 'report') {
          webSocket.sockets.emit(name, data);
        } else {
          webSocket.emit('alert', { msg: name });
        }
      }
      console.log('==========');
    };
    batSimulator.register(batObserver);

    setInterval(async () => {
      batSimulator.update();
    }, 2000);

    console.log('Start Battery Simulator Service');
  }

  /**
   * App entry point
   */
  static async main() {
    await Index.startWebService();
    await Index.startBatService();
  }
}

export default Index;
