import util from 'util';
import path from 'path';
import fs from 'fs';

import webpack from 'webpack';
import merge from 'webpack-merge';

const DIST = path.resolve(__dirname, '../build/bat-simulator');
const ENTRY_POINT = [path.resolve('./script/prod.js')];
const pkg = require('../package.json');

const NODE_ENV = 'production';
const config = merge.smart(Object.assign({}, {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: [[
              '@babel/preset-env',
              {
                targets: { node: 'current' }
              }
            ]]
          }
        }
      }
    ]
  },
  node: {
    __dirname: false
  }
}, {
  target: 'node',
  externals: Object.keys(pkg.dependencies || {}),
  mode: NODE_ENV,
  entry: ENTRY_POINT,
  output: {
    path: DIST,
    filename: 'index.js',
    publicPath: './',
    libraryTarget: 'commonjs2'
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV
    })
  ]
}));

function promisify(func) {
  try {
    return util.promisify(func);
  } catch (err) {
    return { error: err.message };
  }
}

function writeFile(file = '', content) {
  return new Promise((resolve) => {
    fs.writeFile(file, content, (err) => {
      if (err) {
        return resolve({ error: (err.message) ? err.message : err });
      }
      return resolve('Success');
    });
  });
}

async function build() {
  const msg = await promisify(webpack)(config);

  const pkgNew = Object.assign({}, {
    name: pkg.name,
    author: pkg.author || 'neilpipi1985',
    version: pkg.version,
    scripts: { start: 'node index.js' },
    dependencies: pkg.dependencies
  });
  await writeFile(`${DIST}/package.json`, `${JSON.stringify(pkgNew, null, 2)}\r\n`);

  return msg;
}

build().then(() => {
  console.log('Building Success');
}).catch((e) => {
  console.log(e.message);
  process.exit(-1);
});
