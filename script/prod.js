import Index from '../src/index';

Index.main().then(() => {
  console.log('App is running');
}).catch((err) => {
  console.log(err);
  process.exit(-1);
});
