import { assert } from 'chai';

import BatSimulator from '../../src/batsimulator';

describe('BatSimulator', () => {
  const batSimulator = new BatSimulator();

  it('BatSimulator should be a Function', () => {
    assert.isFunction(BatSimulator, 'BatSimulator is a function');
  });

  it('new BatSimulator() should be an Object', () => {
    assert.isObject(batSimulator, 'new BatSimulator() is an object');
  });
});

describe('BatSimulator Static Function', () => {
  it('BatSimulator should has a Battery static function', () => {
    assert.isFunction(BatSimulator.Battery, 'Battery is a BatSimulator static function');
  });

  it('BatSimulator should has a BatObserver function', () => {
    assert.isFunction(BatSimulator.BatObserver, 'BatObserver is a BatSimulator static function');
  });
});

describe('BatSimulator Property', () => {
  const batSimulator = new BatSimulator();

  it('BatSimulator should has a bat property', () => {
    assert.isObject(batSimulator.bat, 'bat is a object');
  });

  it('BatSimulator should has a stableState property', () => {
    assert.isObject(batSimulator.stableState, 'stableState is a object');
  });

  it('BatSimulator should has a chargingState property', () => {
    assert.isObject(batSimulator.chargingState, 'chargingState is a object');
  });

  it('BatSimulator should has a dischargingState property', () => {
    assert.isObject(batSimulator.dischargingState, 'dischargingState is a object');
  });

  it('BatSimulator should has a alarmState property', () => {
    assert.isObject(batSimulator.alarmState, 'alarmState is a object');
  });

  it('BatSimulator should has a batSubject property', () => {
    assert.isObject(batSimulator.batSubject, 'batSubject is a object');
  });
});

describe('BatSimulator Function', () => {
  const batSimulator = new BatSimulator();

  it('BatSimulator should has a register function', () => {
    assert.isFunction(batSimulator.register, 'register is a function');
  });

  it('BatSimulator should has a unregister function', () => {
    assert.isFunction(batSimulator.unregister, 'unregister is a function');
  });

  it('BatSimulator should has a update function', () => {
    assert.isFunction(batSimulator.update, 'update is a function');
  });

  it('BatSimulator should has a getBattery function', () => {
    assert.isFunction(batSimulator.getBattery, 'getBattery is a function');
  });

  it('BatSimulator should has a setBatState function', () => {
    assert.isFunction(batSimulator.setBatState, 'setBatState is a function');
  });

  it('BatSimulator should has a getStableState function', () => {
    assert.isFunction(batSimulator.getStableState, 'getStableState is a function');
  });

  it('BatSimulator should has a getChargingState function', () => {
    assert.isFunction(batSimulator.getChargingState, 'getChargingState is a function');
  });

  it('BatSimulator should has a getDischargingState function', () => {
    assert.isFunction(batSimulator.getDischargingState, 'getDischargingState is a function');
  });

  it('BatSimulator should has a getAlarmState function', () => {
    assert.isFunction(batSimulator.getAlarmState, 'getAlarmState is a function');
  });
});
