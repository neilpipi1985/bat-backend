import { assert } from 'chai';

import Battery from '../../src/batsimulator/battery';

describe('Battery', () => {
  const battery = new Battery();

  it('Battery should be a Function', () => {
    assert.isFunction(Battery, 'Battery is a function');
  });

  it('new Battery() should be an Object', () => {
    assert.isObject(battery, 'new Battery() is an object');
  });
});

describe('Battery Property', () => {
  const battery = new Battery();

  it('Battery should has a deviceId property', () => {
    assert.isString(battery.deviceId, 'deviceId is a string');
  });

  it('Battery should has a deviceType property', () => {
    assert.isString(battery.deviceType, 'deviceType is a string');
  });

  it('Battery should has a DESIGN_VOLT property', () => {
    assert.isNumber(battery.DESIGN_VOLT, 'DESIGN_VOLT is a number');
  });

  it('Battery should has a MAX_CELL_VOLT property', () => {
    assert.isNumber(battery.MAX_CELL_VOLT, 'MAX_CELL_VOLT is a number');
  });

  it('Battery should has a MIN_CELL_VOLT property', () => {
    assert.isNumber(battery.MAX_CELL_VOLT, 'MIN_CELL_VOLT is a number');
  });

  it('Battery should has a OV_TH property', () => {
    assert.isNumber(battery.OV_TH, 'OV_TH is a number');
  });

  it('Battery should has a UV_TH property', () => {
    assert.isNumber(battery.UV_TH, 'UV_TH is a number');
  });

  it('Battery should has a SOC property', () => {
    assert.isNumber(battery.SOC, 'SOC is a number');
  });

  it('Battery should has a CURRENT property', () => {
    assert.isNumber(battery.CURRENT, 'CURRENT is a number');
  });

  it('Battery should has a TEMPERATURE property', () => {
    assert.isNumber(battery.TEMPERATURE, 'TEMPERATURE is a number');
  });

  it('Battery should has a CELL_VOLT property', () => {
    assert.isArray(battery.CELL_VOLT, 'CELL_VOLT is an array');
  });

  it('Battery should has a TOTAL_VOLT property', () => {
    assert.isNumber(battery.TOTAL_VOLT, 'TOTAL_VOLT is an object');
  });

  it('Battery should has a DELTA_VOLT property', () => {
    assert.isNumber(battery.DELTA_VOLT, 'DELTA_VOLT is an object');
  });

  it('Battery should has a ALARM_FLAG property', () => {
    assert.isObject(battery.ALARM_FLAG, 'ALARM_FLAG is an object');
  });
});

describe('Battery Function', () => {
  const battery = new Battery();

  it('Battery should has a refreshVoltValue function', () => {
    assert.isFunction(battery.refreshVoltValue, 'refreshVoltValue is a function');
  });

  it('Battery should has a getInfo Function', () => {
    assert.isFunction(battery.getInfo, 'getInfo is a function');
  });
});
