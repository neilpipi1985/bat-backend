import { assert } from 'chai';

import BatObserver from '../../src/batsimulator/batobserver';

describe('BatObserver', () => {
  const batObserver = new BatObserver();

  it('BatObserver should be a Function', () => {
    assert.isFunction(BatObserver, 'BatObserver is a function');
  });

  it('new BatObserver() should be an Object', () => {
    assert.isObject(batObserver, 'new BatObserver() is an object');
  });
});

describe('BatObserver Function', () => {
  const batObserver = new BatObserver();

  it('BatSubject should has a trigger function', () => {
    assert.isFunction(batObserver.trigger, 'trigger is a function');
  });

  it('BatSubject should has a update function', () => {
    assert.isFunction(batObserver.update, 'update is a function');
  });
});
