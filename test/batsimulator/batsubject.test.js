import { assert } from 'chai';

import BatSubject from '../../src/batsimulator/batsubject';

describe('BatSubject', () => {
  const batSimulator = new BatSubject();

  it('BatSubject should be a Function', () => {
    assert.isFunction(BatSubject, 'BatSubject is a function');
  });

  it('new BatSubject() should be an Object', () => {
    assert.isObject(batSimulator, 'new BatSubject() is an object');
  });
});

describe('BatSubject Property', () => {
  const batSimulator = new BatSubject();

  it('BatSubject should has a consumers property', () => {
    assert.isArray(batSimulator.consumers, 'consumers is an array');
  });
});

describe('BatSubject Function', () => {
  const batSimulator = new BatSubject();

  it('BatSubject should has a subscribe function', () => {
    assert.isFunction(batSimulator.subscribe, 'subscribe is a function');
  });

  it('BatSubject should has a unsubscribe function', () => {
    assert.isFunction(batSimulator.unsubscribe, 'unsubscribe is a function');
  });

  it('BatSubject should has a publish function', () => {
    assert.isFunction(batSimulator.publish, 'publish is a function');
  });
});
