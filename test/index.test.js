import { assert } from 'chai';

import Index from '../src/index';

describe('Index', () => {
  it('Index should be a Function', () => {
    assert.isFunction(Index, 'Index is a function');
  });

  it('Index.main should be an async Function', async () => {
    assert.isFunction(Index.main, 'Index.main is an async function');
  });

  it('Index.startBatService should be an async Function', async () => {
    assert.isFunction(Index.startBatService, 'Index.startBatService is an async function');
  });

  it('Index.startWebService should be an async Function', async () => {
    assert.isFunction(Index.startWebService, 'Index.startWebService is an async function');
  });

  it('Index.startWebSocketService should be an Function', async () => {
    assert.isFunction(Index.startWebSocketService, 'Index.startWebSocketService is an function');
  });
});
